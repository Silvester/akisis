import { IOrder } from 'src/app/shared/models';

const ORDER1: IOrder = {
  id: 1,
  quantity: 200,
  targetPartTime: 80,
  orderNumber: 'FA123',
  deliveryDate: new Date('2021-12-12'),
};

const ORDER2: IOrder = {
  id: 2,
  quantity: 100,
  targetPartTime: 60,
  orderNumber: 'FA124',
  deliveryDate: new Date('2022-12-12'),
};

export const ORDERS = [ORDER1, ORDER2];
