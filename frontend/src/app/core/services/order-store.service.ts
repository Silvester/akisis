import { BehaviorSubject } from 'rxjs';
import { IOrder } from 'src/app/shared/models';

export class OrderStoreService {
  private readonly _orderSoruce = new BehaviorSubject<IOrder>({
    id: -1,
    deliveryDate: new Date(),
    orderNumber: '-1',
    quantity: -1,
    targetPartTime: -1,
  });

  readonly order$ = this._orderSoruce.asObservable();
  constructor() {}
  getOrder(): IOrder {
    return this._orderSoruce.getValue();
  }

  setOrder(order: IOrder): void {
    this._setOrder(order);
  }

  private _setOrder(order: IOrder): void {
    this._orderSoruce.next(order);
  }
}
