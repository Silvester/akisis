import { BehaviorSubject } from 'rxjs';

export class StateService {
  private readonly _loadingStateSource = new BehaviorSubject<boolean>(false);

  readonly loadingState$ = this._loadingStateSource.asObservable();
  constructor() {}

  getLoadingState(): boolean {
    return this._loadingStateSource.getValue();
  }

  isLoading(state: boolean): void {
    this._setLoadingState(state);
  }

  private _setLoadingState(state: boolean): void {
    this._loadingStateSource.next(state);
  }
}
