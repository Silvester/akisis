import { from, Observable, of } from 'rxjs';
import { IOrder } from 'src/app/shared/models';
import { ORDERS } from '../mocks/order.mock';

export class OrderService {
  orders: IOrder[] = [];
  constructor() {
    this.orders = ORDERS;
  }

  getOrders(): IOrder[] {
    return this.orders;
  }
  getOrdersByOrderNumber(orderNumber: string): Observable<IOrder[]> {
    // TODO: add test
    const retVal = this.orders.filter((order) =>
      this.checkIfSearchPhraseIsInCompleteValueCaseIgnore(
        order.orderNumber,
        orderNumber
      )
    );

    return of(retVal);
  }

  private checkIfSearchPhraseIsInCompleteValueCaseIgnore(
    completeValue: string,
    searchPhrase: string
  ): boolean {
    const complete = completeValue.toLowerCase();
    const search = searchPhrase.toLowerCase();
    return complete.includes(search);
  }
}
