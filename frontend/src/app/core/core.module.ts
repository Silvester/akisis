import { NgModule } from '@angular/core';
import { MatToolbarModule } from '@angular/material/toolbar';

import { OrderService } from './services/order.service';
import { StateService } from './services/state.service';
import { HeaderComponent } from './header/header.component';
import { OrderStoreService } from './services/order-store.service';

@NgModule({
  providers: [OrderService, StateService, OrderStoreService],
  imports: [MatToolbarModule],
  declarations: [HeaderComponent],
  exports: [HeaderComponent],
})
export class CoreModule {}
