// Angular Basics
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// Import firebase
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireAuthModule } from '@angular/fire/auth';

/*Custom Modules*/
import { CoreModule } from './core';

/*Mat Modules */
import { MatProgressBarModule } from '@angular/material/progress-bar';

const customModules = [CoreModule];
/*Custom Modules*/

const firebaseConfig = {
  apiKey: 'AIzaSyCNvlsNT0KAhGNw6xGPtxD2aUshUj2dn1E',
  authDomain: 'akisis-553d4.firebaseapp.com',
  projectId: 'akisis-553d4',
  storageBucket: 'akisis-553d4.appspot.com',
  messagingSenderId: '124918728854',
  appId: '1:124918728854:web:fdc1fd1c3f66476b079ab7',
  measurementId: 'G-V8WC8FC3HE',
};

@NgModule({
  declarations: [AppComponent],
  imports: [
    MatProgressBarModule,
    BrowserModule,

    AppRoutingModule,
    ServiceWorkerModule.register('ngsw-worker.js', {
      enabled: environment.production,
      // Register the ServiceWorker as soon as the app is stable
      // or after 30 seconds (whichever comes first).
      registrationStrategy: 'registerWhenStable:30000',
    }),
    BrowserAnimationsModule,
    // Initialize firebase
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFirestoreModule, // firestore
    AngularFireAuthModule, // auth
    AngularFireStorageModule, // storage

    customModules,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
