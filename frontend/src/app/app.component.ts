import { Component, OnInit } from '@angular/core';

import { Observable } from 'rxjs';
import { StateService } from './core/services/state.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  loadingState$: Observable<boolean> | undefined;
  title = 'AKiSiS';
  constructor(private readonly stateService: StateService) {}
  ngOnInit(): void {
    this.loadingState$ = this.stateService.loadingState$;
  }
}
