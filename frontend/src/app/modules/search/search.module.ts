import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MatIconModule } from '@angular/material/icon';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatButtonModule } from '@angular/material/button';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { FormsModule } from '@angular/forms';

import { SearchRoutingModule } from './search-routing.module';

import { SearchPageComponent } from './pages/';
import { OrderListComponent, SearchComponent } from './components/';

var matModules = [
  MatIconModule,
  FormsModule,
  MatFormFieldModule,
  MatButtonModule,
  MatSnackBarModule,
  MatInputModule,
  MatListModule,
];

@NgModule({
  declarations: [SearchPageComponent, SearchComponent, OrderListComponent],
  imports: [matModules, CommonModule, SearchRoutingModule],
})
export class SearchModule {}
