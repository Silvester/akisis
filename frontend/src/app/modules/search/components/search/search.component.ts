import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { OrderService } from 'src/app/core/services/order.service';
import { StateService } from 'src/app/core/services/state.service';
import { IOrder } from 'src/app/shared/models';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css'],
})
export class SearchComponent implements OnInit {
  @Output() myClick = new EventEmitter<IOrder[]>();

  orderNumber = '';
  isDisabled = true;

  orders: IOrder[] = [];
  constructor(
    private readonly snackBar: MatSnackBar,
    private readonly orderService: OrderService,
    private readonly stateService: StateService
  ) {}

  ngOnInit(): void {}
  onSearch(): void {
    //TODO: Suchleiste und button nach oben shcieben
    this.stateService.isLoading(true);
    this.orderService.getOrdersByOrderNumber(this.orderNumber).subscribe({
      next: (orders) => {
        this.stateService.isLoading(false);

        if (orders && orders.length > 0) {
          this.orders = orders;
          this.myClick.emit(orders);
        } else {
          let snackBarRef = this.snackBar.open(
            `Kein Auftrag zu '${this.orderNumber}' gefunden`,
            'Zurücksetzen',
            {
              duration: 3000,
              horizontalPosition: 'right',
              verticalPosition: 'bottom',
            }
          );

          snackBarRef.onAction().subscribe(() => {
            this.orderNumber = '';
          });
        }
      },
      error: (err) => {
        console.log(err);
      },
    });
  }

  onUpdateInput(event: any): void {
    this.orderNumber = event.target.value;
    this.isDisabled = this.orderNumber === '';
  }
}
