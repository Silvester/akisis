import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

import { IOrder } from 'src/app/shared/models';

@Component({
  selector: 'app-order-list',
  templateUrl: './order-list.component.html',
  styleUrls: ['./order-list.component.css'],
})
export class OrderListComponent implements OnInit {
  @Input() orders: IOrder[] | undefined;
  @Output() select = new EventEmitter<IOrder>();

  constructor() {}

  ngOnInit(): void {}

  onSelect(order: IOrder) {
    this.select.emit(order);
  }
}
