import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { OrderStoreService } from 'src/app/core/services/order-store.service';

import { IOrder } from 'src/app/shared/models';

@Component({
  selector: 'app-search-page',
  templateUrl: './search-page.component.html',
  styleUrls: ['./search-page.component.css'],
})
export class SearchPageComponent implements OnInit {
  orders: IOrder[] = [];
  constructor(
    private readonly router: Router,
    private readonly orderStore: OrderStoreService
  ) {}

  ngOnInit(): void {}

  onSearch(order: IOrder[]) {
    this.orders = order;
  }

  onSelectOrder(order: IOrder) {
    this.orderStore.setOrder(order);
    this.router.navigate(['/order']);
  }
}
