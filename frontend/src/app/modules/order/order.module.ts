import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OrderOverviewPageComponent } from './order-overview-page/order-overview-page.component';
import { OrderRoutingModule } from './order-routing.module';

@NgModule({
  declarations: [OrderOverviewPageComponent],
  imports: [CommonModule, OrderRoutingModule],
})
export class OrderModule {}
