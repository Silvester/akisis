import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { OrderStoreService } from 'src/app/core/services/order-store.service';
import { IOrder } from 'src/app/shared/models';

@Component({
  selector: 'app-order-overview-page',
  templateUrl: './order-overview-page.component.html',
  styleUrls: ['./order-overview-page.component.css'],
})
export class OrderOverviewPageComponent implements OnInit {
  order$: Observable<IOrder> | undefined;
  constructor(private readonly orderStore: OrderStoreService) {}

  ngOnInit(): void {
    this.order$ = this.orderStore.order$;
  }
}
