import { ComponentFixture, TestBed } from '@angular/core/testing';
import { OrderStoreService } from 'src/app/core/services/order-store.service';

import { OrderOverviewPageComponent } from './order-overview-page.component';

describe('OrderOverviewPageComponent', () => {
  let component: OrderOverviewPageComponent;
  let fixture: ComponentFixture<OrderOverviewPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [OrderOverviewPageComponent],
      providers: [OrderStoreService],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderOverviewPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
