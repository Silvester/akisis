import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { OrderOverviewPageComponent } from './order-overview-page/order-overview-page.component';

const routes = [{ path: '', component: OrderOverviewPageComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OrderRoutingModule {}
