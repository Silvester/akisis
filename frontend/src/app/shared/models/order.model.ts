import { ICustomer } from './customer.model';

export interface IOrder {
  id: number;
  orderNumber: string;
  quantity: number;
  deliveryDate: Date;
  targetPartTime: number;
  // customer:ICustomer
}
