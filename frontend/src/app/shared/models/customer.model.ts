export interface ICustomer {
  id: number;
  name: string;
  address: IAdress;
}

export interface IAdress {
  id: number;
}
